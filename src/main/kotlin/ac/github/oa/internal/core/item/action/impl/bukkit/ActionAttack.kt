package ac.github.oa.internal.core.item.action.impl.bukkit

import ac.github.oa.internal.core.item.action.IActionEvent
import org.bukkit.entity.Player
import org.bukkit.event.entity.EntityDamageByEntityEvent
import taboolib.module.kether.ScriptContext
import taboolib.platform.util.attacker

object ActionAttack : IActionEvent<EntityDamageByEntityEvent>() {

    override val namespace: String
        get() = "on attack"
    override val event: Class<in EntityDamageByEntityEvent>
        get() = EntityDamageByEntityEvent::class.java

    override fun test(e: EntityDamageByEntityEvent): Player? {
        return e.damager as? Player
    }

    override fun inject(context: ScriptContext, e: EntityDamageByEntityEvent) {
        context.rootFrame().variables()["attacker"] = e.attacker
        context.rootFrame().variables()["damager"] = e.damager
        context.rootFrame().variables()["entity"] = e.entity
    }

}