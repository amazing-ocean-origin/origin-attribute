package ac.github.oa.internal.core.attribute

import kotlin.math.max

class TimeoutAttributeData(val timeout: Long) : AttributeData() {

    val create = System.currentTimeMillis()

    val end: Long
        get() = timeout + create

    val countdown: Long
        get() = max(end - System.currentTimeMillis(), 0)

    override val isValid: Boolean
        get() = timeout == -1L || countdown > 0L

}