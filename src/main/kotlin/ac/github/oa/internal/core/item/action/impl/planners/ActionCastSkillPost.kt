package ac.github.oa.internal.core.item.action.impl.planners

import ac.github.oa.internal.core.item.action.IActionEvent
import ac.github.oa.internal.core.item.action.Source
import com.bh.planners.api.event.PlayerCastSkillEvents
import org.bukkit.entity.Player
import taboolib.module.kether.ScriptContext

@Source("Planners")
object ActionCastSkillPost : IActionEvent<PlayerCastSkillEvents.Post>() {
    override val namespace: String
        get() = "on cast skill post"
    override val event: Class<PlayerCastSkillEvents.Post>
        get() = PlayerCastSkillEvents.Post::class.java

    override fun test(e: PlayerCastSkillEvents.Post): Player {
        return e.player
    }

    override fun inject(context: ScriptContext, e: PlayerCastSkillEvents.Post) {
        context.rootFrame().variables()["skill"] = e.skill.key
    }

}