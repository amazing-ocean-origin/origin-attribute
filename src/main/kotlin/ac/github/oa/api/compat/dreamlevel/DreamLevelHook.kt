package ac.github.oa.api.compat.dreamlevel

import ac.github.oa.OriginAttribute
import ac.github.oa.internal.core.condition.LevelCondition
import cn.kylin.dreamlevel.Main
import cn.kylin.dreamlevel.api.DlApi
import taboolib.common.LifeCycle
import taboolib.common.platform.Awake
import java.util.function.Function

object DreamLevelHook {

    val enable: Boolean
        get() = OriginAttribute.module.isBoolean("dream-level.enable")

    val id: String
        get() = OriginAttribute.module.getString("dream-level.id")!!

    @Awake(LifeCycle.ENABLE)
    fun e() {
        LevelCondition.check = Function {
            if (enable) DlApi.getPlayerData(it.name, id).currentLevel else it.level
        }
    }


}