package ac.github.oa.internal.core.item

import ac.github.oa.internal.core.item.random.RandomFactor
import taboolib.common.platform.function.warning
import taboolib.common5.*
import taboolib.library.configuration.ConfigurationSection
import taboolib.module.nms.ItemTag
import taboolib.module.nms.ItemTagData
import taboolib.module.nms.ItemTagList
import java.util.regex.Pattern

object Translator {

    val regexShort = Pattern.compile("\\d+s")!!

    fun toNBTBase(obj: Any?, factor: RandomFactor): ItemTagData? {
        when (obj) {
            is String -> return if (regexShort.matcher(obj.toString()).matches()) {
                toNBTBase(java.lang.Short.valueOf(obj.toString().substring(0, obj.toString().length - 1)), factor)
            } else {
                val numberConverter = numberConverter(factor.create(obj).toString())
                if (numberConverter is String) {
                    return ItemTagData(numberConverter)
                } else {
                    toNBTBase(numberConverter, factor)
                }

            }

            is Int -> return ItemTagData(obj)
            is Double -> return ItemTagData(obj)
            is Float -> return ItemTagData(obj)
            is Short -> return ItemTagData(obj)
            is Long -> return ItemTagData(obj)
            is Byte -> return ItemTagData(obj)
            is List<*> -> return toNBTList(ItemTagList(), (obj as List<*>?)!!, factor)
            is Map<*, *> -> {
                val nbtCompound = ItemTag()
                obj.forEach { (key, value) -> nbtCompound[key.toString()] = toNBTBase(value, factor) }
                return nbtCompound
            }

            is ConfigurationSection -> {
                val nbtCompound = ItemTag()
                obj.getValues(false).forEach { (key, value) -> nbtCompound[key] = toNBTBase(value, factor) }
                return nbtCompound
            }

            else -> {
                return ItemTagData("Error: " + obj!!)
            }
        }
    }

    fun toNBTList(nbtList: ItemTagList, list: List<*>, factor: RandomFactor): ItemTagList {
        for (obj in list) {
            val base = toNBTBase(obj, factor)
            if (base == null) {
                warning("Invalid Type: " + obj + " [" + obj!!.javaClass.simpleName + "]")
                continue
            }
            nbtList.add(base)
        }
        return nbtList
    }

    fun toNBTCompound(nbt: ItemTag, section: ConfigurationSection, factor: RandomFactor): ItemTag {
        for (key in section.getKeys(false)) {
            val obj = section.get(key)
            val base: ItemTagData?
            if (obj is ConfigurationSection) {
                base = toNBTCompound(ItemTag(), section.getConfigurationSection(key)!!, factor)
            } else {
                base = toNBTBase(obj, factor)
                if (base == null) {
                    warning("Invalid Type: " + obj + " [" + obj!!.javaClass.simpleName + "]")
                    continue
                }
            }
            if (key.endsWith("!!")) {
                nbt[key.substring(0, key.length - 2)] = base
            } else {
                nbt[key] = base
            }
        }
        return nbt
    }

    fun numberConverter(string: String): Any {
        return if (string.endsWith("int")) {
            string.substring(0, string.length - 3).cint
        } else if (string.endsWith("double")) {
            string.substring(string.length - 6).cdouble
        } else if (string.endsWith("boolean")) {
            string.substring(string.length - 7).cbool
        } else if (string.endsWith("float")) {
            string.substring(string.length - 5).cfloat
        } else if (string.endsWith("byte")) {
            string.substring(string.length - 4).cbyte
        } else {
            string
        }
    }

}