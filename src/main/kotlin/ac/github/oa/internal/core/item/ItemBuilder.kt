package ac.github.oa.internal.core.item

import ac.github.oa.api.Debug
import ac.github.oa.api.event.entity.EntityGetItemEvent
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.entity.LivingEntity
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import taboolib.library.xseries.XMaterial
import taboolib.platform.util.giveItem

class ItemBuilder(val item: Item) {

    /**
     * 创建物品
     */
    fun create(target: LivingEntity?, amount: Int = 1, options: Map<String, String> = mutableMapOf()): MutableList<ItemStack> {

        val stacks = mutableListOf<ItemStack>()
        val mapOf = mutableMapOf<String, String>()
        mapOf += options
        (0 until amount).forEach {
            stacks += item.create(target,mapOf) ?: return@forEach
        }
        return stacks
    }

    fun send(target: Player, amount: Int = 1, options: Map<String, String> = mutableMapOf()): List<ItemStack> {
        return send(target, create(target, amount, options))
    }

    fun send(target: Player, itemStack: MutableList<ItemStack>): List<ItemStack> {
        val event = EntityGetItemEvent(target, itemStack, item).apply { call() }
        if (event.isCancelled) return mutableListOf()
        target.giveItem(event.result)
        return event.result
    }

}