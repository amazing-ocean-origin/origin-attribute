package ac.github.oa.util

import ac.github.oa.internal.core.item.ItemInstance
import org.bukkit.entity.Player
import taboolib.module.kether.ScriptContext


const val NAMESPACE = "OriginAttribute"

val namespaces = listOf(NAMESPACE, "kether")

fun ScriptContext.itemInstance(): ItemInstance {
    return get<ItemInstance>("@ItemInstance")!!
}

fun ScriptContext.player(): Player {
    return sender!!.castSafely<Player>()!!
}