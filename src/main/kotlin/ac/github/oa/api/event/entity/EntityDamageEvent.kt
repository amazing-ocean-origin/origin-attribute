package ac.github.oa.api.event.entity

import ac.github.oa.internal.base.enums.PriorityEnum
import ac.github.oa.internal.base.event.impl.DamageMemory
import taboolib.platform.type.BukkitProxyEvent

/**
 * type 0 = pre 1 = post
 */
class EntityDamageEvent(val damageMemory: DamageMemory, var priorityEnum: PriorityEnum) : BukkitProxyEvent() {

    val attacker = damageMemory.attacker

    val victim = damageMemory.victim

    override val allowCancelled: Boolean
        get() = true

    val isPre: Boolean
        get() = priorityEnum == PriorityEnum.PRE

    val isPost: Boolean
        get() = priorityEnum == PriorityEnum.POST


}
