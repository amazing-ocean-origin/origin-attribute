package ac.github.oa.api

import ac.github.oa.api.event.entity.EntityDamageEvent
import ac.github.oa.internal.base.enums.PriorityEnum
import taboolib.common.platform.event.EventPriority
import taboolib.common.platform.event.SubscribeEvent
import taboolib.common.platform.function.info

object Debug {

    var enable = false


    @SubscribeEvent(ignoreCancelled = true, priority = EventPriority.MONITOR)
    fun e(e: EntityDamageEvent) {
        if (enable && e.priorityEnum == PriorityEnum.POST) {
            info("Damage memory ${e.damageMemory.attacker.name} damage ${e.damageMemory.victim.name} value ${e.damageMemory.totalDamage}")
            e.damageMemory.damageSources<Any>().forEach {
                info("- ${it.any} = ${it.value}")
            }
        }
    }

    fun log(message: String) {
        if (enable) {
            info(message)
        }
    }

}