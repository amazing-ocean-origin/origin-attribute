package ac.github.oa.internal.core.kether

import ac.github.oa.util.NAMESPACE
import ac.github.oa.util.itemInstance
import taboolib.module.kether.KetherParser
import taboolib.module.kether.actionNow
import taboolib.module.kether.script
import taboolib.module.kether.scriptParser

object ActionLoreEdit {

    /**
     * lore set [line: Int] [text: String]
     * lore clear
     * lore add [text: String]
     * lore del [line: Int]
     * lore insert [line: Int] [text: String]
     * lore replace [line: Int] [text: String] [text: String]
     * lore replaces [text: String] [text: String]
     */
    @KetherParser(["lore"], namespace = NAMESPACE, shared = true)
    fun parser() = scriptParser {
        when (it.nextToken()) {
            "set" -> actionNow {
                val item = script().itemInstance().itemStack
                item.itemMeta?.lore?.set(it.nextInt(), it.nextToken())
            }

            "clear" -> actionNow {
                val item = script().itemInstance().itemStack
                item.itemMeta?.lore?.clear()
            }

            "add" -> actionNow {
                val item = script().itemInstance().itemStack
                item.itemMeta?.lore?.add(it.nextToken())
            }

            "del", "delete" -> actionNow {
                val item = script().itemInstance().itemStack
                item.itemMeta?.lore?.removeAt(it.nextInt())
            }

            "insert" -> actionNow {
                val item = script().itemInstance().itemStack
                item.itemMeta?.lore?.add(it.nextInt(), it.nextToken())
            }

            "replaces" -> actionNow {
                val item = script().itemInstance().itemStack
                val lore = item.itemMeta?.lore!!
                val quest = it
                lore.forEach {
                    it.replace(quest.nextToken(), quest.nextToken())
                }
            }

            "replace" -> actionNow {
                val item = script().itemInstance().itemStack
                val lore = item.itemMeta?.lore!!
                val line = it.nextInt()
                lore.set(line, lore[line].replace(it.nextToken(), it.nextToken()))
            }

            else -> {
                error("要出来哩！")
            }
        }

    }

}