package ac.github.oa.internal.base.event.impl

import org.bukkit.inventory.ItemStack
import taboolib.module.nms.getItemTag
import taboolib.platform.util.isAir
import taboolib.platform.util.isNotAir

val DamageMemory.isPhysics: Boolean
    get() = this.cause == "physics"

val DamageMemory.isMagic: Boolean
    get() = this.cause == "magic"

val DamageMemory.isProjectile: Boolean
    get() = this.cause == "projectile"

val DamageMemory.isArrow: Boolean
    get() = this.event.isArrow

val ItemStack.customCause: String
    get() = if (isNotAir()) getItemTag()["cause"]?.asString() ?: "physics" else "physics"