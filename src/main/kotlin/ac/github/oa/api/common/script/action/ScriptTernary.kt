package ac.github.oa.api.common.script.action

import ac.github.oa.api.common.script.InlineScript
import ac.github.oa.api.common.script.InlineScriptContext
object ScriptTernary : InlineScript {

    override val id: String
        get() = "t"

    override fun handleScript(argument: String, context: InlineScriptContext): String? {
        val split = argument.split(" ? ")
        val expression = split[0]
        val compareSymbol = CompareSymbol.getSymbol(expression) ?: error(expression)

        val formula = expression.split(" ${compareSymbol.name} ")

        val apply = compareSymbol.handle(formula[0], formula[1])
        val results = split.subList(1, split.size).joinToString(" ? ").split(" : ")
        return if (apply) results[0] else results[1]
    }

}