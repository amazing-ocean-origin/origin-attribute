package ac.github.oa.api.event.item

import ac.github.oa.internal.core.attribute.AttributeData
import ac.github.oa.internal.core.attribute.equip.AdaptItem
import org.bukkit.entity.LivingEntity
import taboolib.platform.type.BukkitProxyEvent

class ItemLoadEvent(val entity: LivingEntity?, val item: AdaptItem): BukkitProxyEvent() {
}