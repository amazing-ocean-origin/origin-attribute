package ac.github.oa.internal.core.attribute

import ac.github.oa.OriginAttribute
import ac.github.oa.api.event.plugin.OriginPluginReloadEvent
import org.bukkit.entity.Player
import org.bukkit.event.player.PlayerPreLoginEvent
import taboolib.common.LifeCycle
import taboolib.common.TabooLibCommon
import taboolib.common.io.getInstance
import taboolib.common.io.runningClasses
import taboolib.common.platform.Awake
import taboolib.common.platform.event.SubscribeEvent
import taboolib.common.platform.function.info
import taboolib.module.configuration.Config
import taboolib.module.configuration.Configuration
import java.util.*

object AttributeManager {

    val attributeInstances = mutableListOf<Attribute>()
    val usableAttributes = sortedMapOf<Int, Attribute>()

    val attributeRegistry = mutableListOf<String>()


    @Config("attribute/config.yml")
    lateinit var config: Configuration

    private val table = mutableMapOf<UUID, AttributeData>()

    fun remove(uuid: UUID) {
        table.remove(uuid)
    }

    fun set(uuid: UUID, data: AttributeData) {
        table[uuid] = data
    }

    fun get(player: Player): AttributeData {
        return get(player.uniqueId)
    }

    fun get(uuid: UUID): AttributeData {
        return table[uuid] ?: AttributeData()
    }

    fun getAttribute(clazz: Class<*>): Attribute {
        return attributeInstances.first { it::class.java == clazz }
    }

    fun getAttribute(name: String): Attribute {
        return attributeInstances.first { it.toName() == name }
    }

    fun getAttribute(index: Int): Attribute {
        return usableAttributes.values.first { it.getPriority() == index }
    }

    @Awake(LifeCycle.LOAD)
    fun initClass() {
        runningClasses.forEach {
            if (Attribute::class.java.isAssignableFrom(it) && !it.isAnnotationPresent(Abstract::class.java)) {
                registerAttribute(it.getInstance()?.get() as? Attribute ?: return@forEach)
            }
        }
    }

    @Awake(LifeCycle.ENABLE)
    fun setupClass() {
        attributeRegistry.clear()
        attributeRegistry += OriginAttribute.config.getStringList("attributes")
        attributeInstances.forEach { loadAttribute(it) }
    }

    @SubscribeEvent
    fun e(e: PlayerPreLoginEvent) {
        if (TabooLibCommon.getLifeCycle() != LifeCycle.ACTIVE) {
            e.disallow(PlayerPreLoginEvent.Result.KICK_OTHER, "§4请等待服务器启动...")
        }
    }

    fun registerAttribute(clazz: Class<*>) {
        registerAttribute(clazz.getDeclaredConstructor().newInstance() as Attribute)
    }

    fun registerAttribute(attribute: Attribute) {
        attributeInstances += attribute
    }

    fun loadAttribute(attribute: Attribute) {
        val priority = getPriority(attribute)
        info("Load Attribute ${attribute.toName()} index $priority")
        if (priority != -1) {
            attribute.setPriority(priority)
            usableAttributes[priority] = attribute
            enableAttribute(attribute)
        }
    }

    @SubscribeEvent
    fun e(e: OriginPluginReloadEvent) {
        usableAttributes.values.forEach { it.onReload() }
    }

    fun enableAttribute(attribute: Attribute) {
        attribute.onLoad()
    }

    fun getPriority(attribute: Attribute): Int {
        return attributeRegistry.indexOf(attribute.toName())
    }

}
