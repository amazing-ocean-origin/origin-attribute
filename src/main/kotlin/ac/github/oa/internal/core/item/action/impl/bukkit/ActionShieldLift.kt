package ac.github.oa.internal.core.item.action.impl.bukkit

import ac.github.oa.internal.core.item.action.IActionEvent
import org.bukkit.entity.Player
import org.bukkit.event.player.PlayerInteractEvent
import taboolib.module.kether.ScriptContext
import taboolib.platform.util.isOffhand
import taboolib.platform.util.isRightClick

object ActionShieldLift : IActionEvent<PlayerInteractEvent>() {

    override val namespace: String
        get() = "on shield lift"
    override val event: Class<PlayerInteractEvent>
        get() = PlayerInteractEvent::class.java

    override fun test(e: PlayerInteractEvent): Player? {
        return if (e.player.inventory.itemInMainHand.type.name.contains("SHIELD") && e.isRightClick() && e.isOffhand()) {
            return e.player
        } else null
    }

    override fun inject(context: ScriptContext, e: PlayerInteractEvent) {

    }

}