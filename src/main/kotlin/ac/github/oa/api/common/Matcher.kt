package ac.github.oa.api.common

import taboolib.common5.Coerce

interface Matcher {

    // 攻击力 10 - 20
    class Range(val parse: String, val delimiters: String = " - ") : Matcher {

        private val split = parse.split(delimiters)
        val pos1 = Single(split[0])
        val pos2 = Single(split.getOrElse(1) { split[0] })

        val min: Double
            get() = pos1.toDouble()

        val max: Double
            get() = pos2.toDouble()

    }

    // 暴击伤害 30
    class Single(var parse: String) : Matcher {
        private var string = parse

        val main = StringBuilder()
        val sub = StringBuilder()

        val amount: Double
            get() = mainAmount + extraAmount

        var mainAmount = 0.0
        var extraAmount = 0.0

        fun initial() {
            regex.forEach { string = string.replace(it, "") }
            var extraFlag = false

            var index = 0
            while (index < string.length) {
                val it = string[index]

                if (it == '(') {
                    extraFlag = true
                }

                if (it == ')') {
                    extraFlag = false
                }

                if (it == '+' || it == '-' || it == '.' || it in '0'..'9') {
                    if (!extraFlag) {
                        main.append(it)
                    } else {
                        sub.append(it)
                    }
                }

                index++
            }

            mainAmount = Coerce.toDouble(main)
            extraAmount = Coerce.toDouble(sub)
        }

        init {
            initial()
        }

        fun toInt() = Coerce.toInteger(amount)

        fun toDouble() = amount

    }


    companion object {

        val regex = mutableListOf(
            Regex("\\s"), Regex("§.")
        )

        fun replaceRegex(template: String): String {
            var string = template
            regex.forEach {
                string = string.replace(it, "")
            }
            return string
        }

        fun range(str: String, delimiters: String = " - "): Range {
            return Range(str, delimiters)
        }

        fun single(str: String): Single {
            return Single(str)
        }

        fun parse(str: String): Matcher {
            val string = replaceRegex(str)
            if (str.contains(" - ")) {
                return Range(string)
            }

            return Single(string)
        }

    }

}