package ac.github.oa.internal.core.item.action.impl.planners

import ac.github.oa.internal.core.item.action.IActionEvent
import ac.github.oa.internal.core.item.action.Source
import com.bh.planners.api.event.PlayerCastSkillEvents
import org.bukkit.entity.Player
import taboolib.module.kether.ScriptContext

@Source("Planners")
object ActionCastSkillPre : IActionEvent<PlayerCastSkillEvents.Pre>() {

    override val namespace: String
        get() = "on cast skill pre"
    override val event: Class<PlayerCastSkillEvents.Pre>
        get() = PlayerCastSkillEvents.Pre::class.java

    override fun test(e: PlayerCastSkillEvents.Pre): Player {
        return e.player
    }

    override fun inject(context: ScriptContext, e: PlayerCastSkillEvents.Pre) {
        context.rootFrame().variables()["skill"] = e.skill.key
    }

}