package ac.github.oa.api.common.script

interface InlineScript {

    val id: String

    fun handleScript(argument: String, context: InlineScriptContext): String?

}