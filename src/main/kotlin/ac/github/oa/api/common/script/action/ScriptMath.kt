package ac.github.oa.api.common.script.action

import ac.github.oa.OriginAttribute
import ac.github.oa.api.common.script.InlineScript
import ac.github.oa.api.common.script.InlineScriptContext
import taboolib.common5.Coerce

class ScriptMath {

    class ScriptMax : InlineScript {
        override val id: String
            get() = "max"

        override fun handleScript(argument: String, context: InlineScriptContext): String? {

            return OriginAttribute.decimalFormat.format(
                argument.trim().split(",").map { Coerce.toDouble(it) }.maxOf { it }
            )

        }

    }

    class ScriptMin : InlineScript {
        override val id: String
            get() = "min"

        override fun handleScript(argument: String, context: InlineScriptContext): String? {

            return OriginAttribute.decimalFormat.format(
                argument.trim().split(",").map { Coerce.toDouble(it) }.minOf { it }
            )

        }

    }

    class InferInt: InlineScript {
        override val id: String
            get() = "toInt"

        override fun handleScript(argument: String, context: InlineScriptContext): String? {
            return Coerce.toInteger(argument).toString()
        }

    }
    class InferDouble: InlineScript {
        override val id: String
            get() = "toDouble"

        override fun handleScript(argument: String, context: InlineScriptContext): String? {
            return Coerce.toInteger(argument).toString()
        }

    }
}