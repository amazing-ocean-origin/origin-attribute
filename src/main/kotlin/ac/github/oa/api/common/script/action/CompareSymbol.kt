package ac.github.oa.api.common.script.action

import java.math.BigDecimal

interface CompareSymbol {

    val name: String

    fun apply(decimal1: BigDecimal, decimal2: BigDecimal): Boolean

    fun handle(left: String, right: String): Boolean

    companion object {


        val symbols = arrayListOf(
            CompareSymbolEQ, CompareSymbolGE, CompareSymbolGT, CompareSymbolLE, CompareSymbolLT, CompareSymbolNE
        )

        fun getSymbol(expression: String): CompareSymbol? {
            return symbols.filter { expression.contains(" ${it.name} ") }.firstOrNull()
        }

    }

}
