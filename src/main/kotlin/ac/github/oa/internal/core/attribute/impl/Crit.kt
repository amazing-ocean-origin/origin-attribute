package ac.github.oa.internal.core.attribute.impl

import ac.github.oa.api.event.entity.EntityDamageEvent
import ac.github.oa.internal.base.event.EventMemory
import ac.github.oa.internal.base.event.impl.DamageMemory
import ac.github.oa.internal.core.attribute.AbstractAttribute
import ac.github.oa.internal.core.attribute.Attribute
import ac.github.oa.internal.core.attribute.AttributeData
import ac.github.oa.internal.core.attribute.AttributeType
import taboolib.common.platform.event.SubscribeEvent
import taboolib.common.platform.function.info
import taboolib.common.util.random
import taboolib.common5.cbool

object Crit : AbstractAttribute() {
    override val types: Array<AttributeType>
        get() = arrayOf(AttributeType.ATTACK)


    val change = object : Attribute.Entry() {

        override val type: Attribute.Type
            get() = Attribute.Type.SINGLE

        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            if (memory is DamageMemory) {

                if (memory.labels.containsKey("@Crit")) {
                    memory.setLabel(this, memory.labels["@Crit"].cbool)
                    return
                }

                // 暴击抵抗
                val critChanceResistance =
                    memory.victimData.getData(this@Crit.index, critChanceResistance.index).get(critChanceResistance)

                val isCrit = random((data.get(this) - critChanceResistance) / 100)
                memory.setLabel(this, isCrit)
                memory.setLabel("@Crit", isCrit)
            }
        }
    }

    val damage = object : Attribute.Entry() {

        override val type: Attribute.Type
            get() = Attribute.Type.SINGLE

        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            if (memory is DamageMemory && memory.labels[change] == true) {
                // 爆伤抵抗
                val critChanceResistance =
                    memory.victimData.getData(this@Crit.index, critDamageResistance.index).get(critDamageResistance)
                val scope = (data.get(0) - critChanceResistance) / 100

                // 给伤害来源先乘以倍数
                memory.addDamage(this, memory.countDamageSources { it.value * scope })
            }
        }

    }

    val critChanceResistance = object : Attribute.Entry() {

        override val type: Attribute.Type
            get() = Attribute.Type.SINGLE

        override fun handler(memory: EventMemory, data: AttributeData.Data) {}

    }


    val critDamageResistance = object : Attribute.Entry() {

        override val type: Attribute.Type
            get() = Attribute.Type.SINGLE

        override fun handler(memory: EventMemory, data: AttributeData.Data) {}

    }


}
