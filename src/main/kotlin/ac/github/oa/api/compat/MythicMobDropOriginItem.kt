package ac.github.oa.api.compat

import ac.github.oa.internal.core.item.ItemPlant
import io.lumine.xikage.mythicmobs.adapters.AbstractEntity
import io.lumine.xikage.mythicmobs.adapters.AbstractLocation
import io.lumine.xikage.mythicmobs.adapters.bukkit.BukkitAdapter
import io.lumine.xikage.mythicmobs.api.bukkit.events.MythicDropLoadEvent
import io.lumine.xikage.mythicmobs.api.bukkit.events.MythicMechanicLoadEvent
import io.lumine.xikage.mythicmobs.drops.Drop
import io.lumine.xikage.mythicmobs.io.MythicLineConfig
import io.lumine.xikage.mythicmobs.skills.ITargetedEntitySkill
import io.lumine.xikage.mythicmobs.skills.ITargetedLocationSkill
import io.lumine.xikage.mythicmobs.skills.SkillMechanic
import io.lumine.xikage.mythicmobs.skills.SkillMetadata
import io.lumine.xikage.mythicmobs.skills.mechanics.CustomMechanic
import io.lumine.xikage.mythicmobs.skills.mechanics.DropItemMechanic
import io.lumine.xikage.mythicmobs.skills.placeholders.parsers.PlaceholderInt
import taboolib.common.platform.event.SubscribeEvent
import taboolib.common.platform.function.submit
import taboolib.common5.eqic

class MythicMobDropOriginItem(line: String, mlc: MythicLineConfig) : DropItemMechanic(line,mlc) {

    companion object {

    }

    private var amount: PlaceholderInt = mlc.getPlaceholderInteger("amount", "a")
    val key: String = mlc.getString("key")

    override fun castAtEntity(skillMetadata: SkillMetadata, abstractEntity: AbstractEntity): Boolean {
        return castAtLocation(skillMetadata, abstractEntity.location)
    }

    override fun castAtLocation(data: SkillMetadata, location: AbstractLocation): Boolean {
        val bukkitLocation = BukkitAdapter.adapt(location)
        submit {
            IntRange(0, amount.get()).map { ItemPlant.build(null, key) }.forEach {
                bukkitLocation.world!!.dropItem(bukkitLocation, it ?: return@forEach)
            }
        }
        return true
    }
}