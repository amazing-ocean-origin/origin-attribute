package ac.github.oa.internal.core.attribute.impl

import ac.github.oa.internal.base.event.EventMemory
import ac.github.oa.internal.base.event.impl.DamageMemory
import ac.github.oa.internal.core.attribute.AbstractAttribute
import ac.github.oa.internal.core.attribute.Attribute
import ac.github.oa.internal.core.attribute.AttributeData
import ac.github.oa.internal.core.attribute.AttributeType

object Jump : AbstractAttribute() {
    override val types: Array<AttributeType>
        get() = arrayOf(AttributeType.ATTACK)

    val damage = object : Attribute.Entry() {

        override val type: Attribute.Type
            get() = Attribute.Type.RANGE

        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            memory as? DamageMemory ?: return
            if (memory.attacker.fallDistance > 0) {
                memory.addDamage(this, data.random())
            }
        }

    }

    val addon = object : Attribute.Entry() {

        override val type: Attribute.Type
            get() = Attribute.Type.SINGLE

        val default: Double
            get() = option?.getDouble("default") ?: 0.0

        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            memory as? DamageMemory ?: return
            if (memory.attacker.fallDistance > 0) {
                memory.addDamage(this, data.random())
                var count = 0.0
                memory.dynamicDamageSource {
                    count += it.value * (data.get() / 100 + default)
                }
                memory.addDamage(count)
            }
        }

    }

}