package ac.github.oa.internal.core.attribute.impl

import ac.github.oa.OriginAttribute.proExpression
import ac.github.oa.internal.base.event.EventMemory
import ac.github.oa.internal.base.event.impl.DamageMemory
import ac.github.oa.internal.core.attribute.AbstractAttribute
import ac.github.oa.internal.core.attribute.Attribute
import ac.github.oa.internal.core.attribute.AttributeData
import ac.github.oa.internal.core.attribute.AttributeType
import org.bukkit.entity.Player

object Defence : AbstractAttribute() {

    override val types: Array<AttributeType>
        get() = arrayOf(AttributeType.DEFENSE)

    val physical = object : AbstractDefenceEntry() {

        override val type: Attribute.Type
            get() = Attribute.Type.RANGE

        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            memory as DamageMemory
            var count = 0.0
            // 统计所有物理伤害的值
            memory.damageSources<Any>().forEach {
                if (it.any == Damage.physical) {
                    count += it.value
                }
            }
            if (proExpression) {
                // 高阶公式
                val denominator = data.get(this)+count
                if (denominator == 0.0) {
                    memory.addDamage(this, 0.0)
                } else {
                    memory.addDamage(this, -((data.get(this)*count)/(data.get(this)+count)).coerceAtMost(count))
                }
            } else {
                // 最高减免的数值
                memory.addDamage(this, -data.get(this).coerceAtMost(count))
            }
        }

    }

    val magic = object : AbstractDefenceEntry() {

        override val type: Attribute.Type
            get() = Attribute.Type.RANGE

        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            memory as DamageMemory
            var count = 0.0
            // 统计所有物理伤害的值
            memory.damageSources<Any>().forEach {
                if (it.any == Damage.magic) {
                    count += it.value
                }
            }
            if (proExpression) {
                // 高阶公式
                val denominator = data.get(this)+count
                if (denominator == 0.0) {
                    memory.addDamage(this, 0.0)
                } else {
                    memory.addDamage(this, -((data.get(this)*count)/(data.get(this)+count)).coerceAtMost(count))
                }
            } else {
                // 最高减免的数值
                memory.addDamage(this, -data.get(this).coerceAtMost(count))
            }
        }

    }

    val addon = DefaultAddonImpl()

    val pvpDefence = object : AbstractDefenceEntry() {

        override val type: Attribute.Type
            get() = Attribute.Type.RANGE

        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            memory as DamageMemory
            if (memory.attacker is Player) {
                memory.addDamage(this, -(data.get(0) * scale))
            }
        }
    }

    val pveDefence = object : AbstractDefenceEntry() {
        override val type: Attribute.Type
            get() = Attribute.Type.RANGE

        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            memory as DamageMemory
            if (memory.attacker !is Player) {
                memory.addDamage(this, -(data.get(0) * scale))
            }
        }
    }


    class DefaultAddonImpl : AbstractDefenceEntry() {

        override val type: Attribute.Type
            get() = Attribute.Type.SINGLE

        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            memory as DamageMemory

            val count = memory.damageSources<Any>().sumOf { if (it.any == physical || it.any == magic) it.value else 0.0 }

            memory.addDamage(this, -(count * data.get(this) / 100 * scale))
        }

    }

    abstract class AbstractDefenceEntry : Attribute.Entry() {

        val scale: Double
            get() = node.toRoot().getDouble("${name}.scale", 1.0)

    }


}
