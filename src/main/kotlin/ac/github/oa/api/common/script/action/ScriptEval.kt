package ac.github.oa.api.common.script.action

import ac.github.oa.OriginAttribute
import ac.github.oa.api.common.script.InlineScript
import ac.github.oa.api.common.script.InlineScriptContext
import ac.github.oa.util.ReportUtil
import taboolib.common.platform.function.info
import taboolib.common.platform.function.warning
import taboolib.platform.BukkitPlugin
import java.util.logging.Level

object ScriptEval : InlineScript {

    override val id: String
        get() = "eval"

    override fun handleScript(argument: String, context: InlineScriptContext): String? {
        return try {
            val result = ReportUtil.getResult(argument.replace("\\s", ""))

            OriginAttribute.decimalFormat.format(result)
        } catch (e: Exception) {
            warning("错误的表达式 $argument")
            "-1"
        }

    }
}