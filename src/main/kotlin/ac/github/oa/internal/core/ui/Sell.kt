package ac.github.oa.internal.core.ui

import org.bukkit.inventory.ItemStack
import taboolib.library.configuration.ConfigurationSection
import taboolib.library.xseries.getItemStack
import taboolib.module.configuration.Config
import taboolib.module.configuration.Configuration

object Sell {

    @Config("sell.yml", migrate = true)
    lateinit var config: Configuration

    val root: ConfigurationSection
        get() = config.getConfigurationSection("sell")!!

    val title: String
        get() = root.getString("title")!!

    val itemP: String
        get() = root.getString("\$item")!!

    val icon: ItemStack
        get() = root.getItemStack("icon-#")!!

    val submitItem0: ItemStack
        get() = root.getItemStack("icon-submit0")!!

    val submitItem1: ItemStack
        get() = root.getItemStack("icon-submit1")!!

    val noneKeywords: List<String>
        get() = root.getStringList("none")

    val keywords: List<String>
        get() = root.getStringList("keywords")

    val default: Double
        get() = root.getDouble("default", 1.0)

    val ITEMS
        get() = root.getIntegerList("slots")

    val FLAP
        get() = root.getIntegerList("icon-#.slots")

    val SUBMIT
        get() = root.getInt("slot")


}