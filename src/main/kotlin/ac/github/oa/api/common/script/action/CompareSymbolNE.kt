package ac.github.oa.api.common.script.action

import taboolib.common5.Coerce
import java.math.BigDecimal

object CompareSymbolNE : CompareSymbol {
    override val name: String
        get() = "!="

    override fun apply(decimal1: BigDecimal, decimal2: BigDecimal): Boolean {
        return decimal2.compareTo(decimal1) != 0
    }
    override fun handle(left: String, right: String): Boolean {
        return Coerce.toDouble(left) != Coerce.toDouble(right)
    }
}