package ac.github.oa.util

import org.bukkit.entity.LivingEntity
import taboolib.library.reflex.Reflex.Companion.getProperty
import taboolib.library.reflex.Reflex.Companion.setProperty
import taboolib.module.nms.MinecraftVersion
import taboolib.platform.util.removeMeta
import taboolib.platform.util.setMeta


fun doDamage(source: LivingEntity?, entity: LivingEntity, damage: Double) {
    entity.noDamageTicks = 0

    // 如果实体血量 - 预计伤害值 < 0 提前设置击杀者
    if (source != null && entity.health - damage <= 0) {
        entity.setKiller(source)
    }
    entity.damage(damage)
    entity.noDamageTicks = 0
}

fun LivingEntity.setKiller(source: LivingEntity) {
    when (MinecraftVersion.major) {
        // 1.12.* 1.16.*
        4, 8 -> setProperty("entity/killer", source.getProperty("entity"))
        // 1.18.* 1.19.*
        7, 9 -> setProperty("entity/bc", source.getProperty("entity"))
        // 1.18.* 1.19.* bd
        10, 11 -> setProperty("entity/bd", source.getProperty("entity"))

    }
}