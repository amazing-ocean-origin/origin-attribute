package ac.github.oa.command

import ac.github.oa.OriginAttribute
import ac.github.oa.api.Debug
import ac.github.oa.api.ItemAPI
import ac.github.oa.api.OriginAttributeAPI
import ac.github.oa.api.event.plugin.OriginPluginReloadEvent
import ac.github.oa.internal.core.item.ItemPlant
import ac.github.oa.internal.core.item.random.RandomPlant
import ac.github.oa.internal.core.ui.DecomposeUI
import ac.github.oa.internal.core.ui.InfoUI
import ac.github.oa.internal.core.ui.RepairUI
import ac.github.oa.internal.core.ui.SellUI
import org.bukkit.Bukkit
import taboolib.common.platform.ProxyCommandSender
import taboolib.common.platform.ProxyPlayer
import taboolib.common.platform.command.CommandBody
import taboolib.common.platform.command.CommandHeader
import taboolib.common.platform.command.mainCommand
import taboolib.common.platform.command.subCommand
import taboolib.expansion.createHelper
import taboolib.platform.util.onlinePlayers

@CommandHeader("rpg", aliases = ["oa", "rpgo", "originattribute"])
object Command {

    @CommandBody
    val helper = mainCommand {
        createHelper()
    }

    @CommandBody
    val item = CommandItem

    @CommandBody
    val map = CommandMap

    @CommandBody
    val reload = subCommand {
        execute<ProxyCommandSender> { sender, _, _ ->
            OriginAttribute.module.reload()
            ItemPlant.init()
            RandomPlant.init()
            OriginAttribute.config.reload()
            onlinePlayers.forEach { ItemAPI.checkUpdate(it,it.inventory) }
            OriginPluginReloadEvent().call()
            sender.sendMessage("reload successful.")
        }
    }


    @CommandBody
    val info = subCommand {
        dynamic(comment = "viewer") {
            suggestion<ProxyCommandSender> { _, _ -> Bukkit.getOnlinePlayers().map { it.name } }

            execute<ProxyCommandSender> { _, _, argument ->
                Bukkit.getPlayerExact(argument)?.let {
                    InfoUI(it, it).open()
                }
            }

            dynamic(comment = "target") {
                suggestion<ProxyCommandSender> { _, _ -> Bukkit.getOnlinePlayers().map { it.name } }

                execute<ProxyCommandSender> { _, context, argument ->
                    InfoUI(Bukkit.getPlayerExact(context["viewer"])!!, Bukkit.getPlayerExact(argument)!!).open()
                }

            }

        }

    }

    @CommandBody
    val sell = subCommand {
        dynamic(comment = "viewer") {
            suggestion<ProxyCommandSender> { _, _ -> Bukkit.getOnlinePlayers().map { it.name } }

            execute<ProxyCommandSender> { _, _, argument ->
                val player = Bukkit.getPlayerExact(argument) ?: return@execute
                SellUI(player).build()
            }

        }

    }

    @CommandBody
    val repair = subCommand {
        dynamic(comment = "viewer") {
            suggestion<ProxyCommandSender> { _, _ -> Bukkit.getOnlinePlayers().map { it.name } }

            execute<ProxyCommandSender> { _, _, argument ->
                val player = Bukkit.getPlayerExact(argument) ?: return@execute
                RepairUI(player).open()
            }

        }

    }

    @CommandBody
    val decompose = subCommand {
        dynamic(comment = "viewer") {
            suggestion<ProxyCommandSender> { _, _ -> Bukkit.getOnlinePlayers().map { it.name } }

            execute<ProxyCommandSender> { _, _, argument ->
                val player = Bukkit.getPlayerExact(argument) ?: return@execute
                DecomposeUI(player).open()
            }

        }

    }

    @CommandBody
    val heal = subCommand {
        execute<ProxyPlayer> { sender, _, _ ->
            val player = Bukkit.getPlayerExact(sender.name) ?: return@execute
            OriginAttributeAPI.asyncHeal(player)
        }
        dynamic(comment = "player") {
            suggestion<ProxyCommandSender> { _, _ -> Bukkit.getOnlinePlayers().map { it.name } }

            execute<ProxyCommandSender> { _, _, argument ->
                val player = Bukkit.getPlayerExact(argument) ?: return@execute
                OriginAttributeAPI.asyncHeal(player)
            }
        }
    }

    @CommandBody
    val debug = subCommand {
        execute<ProxyCommandSender> { sender, _, _ ->
            Debug.enable = !Debug.enable
            sender.sendMessage("${Debug.enable} # Debug toggle successful.")
        }

    }



}
