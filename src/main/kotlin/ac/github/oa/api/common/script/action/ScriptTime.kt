package ac.github.oa.api.common.script.action

import ac.github.oa.OriginAttribute
import ac.github.oa.api.common.script.InlineScript
import ac.github.oa.api.common.script.InlineScriptContext
import taboolib.common5.Coerce
import java.time.LocalDateTime

object ScriptTime : InlineScript {
    override val id: String
        get() = "time"

    override fun handleScript(argument: String, context: InlineScriptContext): String? {

        val millis = if (argument.isEmpty()) {
            System.currentTimeMillis()
        } else {
            Coerce.toLong(argument)
        }

        return OriginAttribute.simpleDateFormat.format(millis)
    }
}