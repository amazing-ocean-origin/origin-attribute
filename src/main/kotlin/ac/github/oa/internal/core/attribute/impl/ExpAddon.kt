package ac.github.oa.internal.core.attribute.impl

import ac.github.oa.api.OriginAttributeAPI
import ac.github.oa.internal.base.event.EventMemory
import ac.github.oa.internal.core.attribute.*
import org.bukkit.event.player.PlayerExpChangeEvent
import taboolib.common.platform.event.SubscribeEvent
import taboolib.platform.util.sendActionBar

object ExpAddon : AbstractAttribute() {

    override val types: Array<AttributeType>
        get() = arrayOf(AttributeType.OTHER)

    @SubscribeEvent
    fun e(e: PlayerExpChangeEvent) {
        if (index == -1) return
        val amount = e.amount

        val attributeData = OriginAttributeAPI.getAttributeData(e.player)
        val value = attributeData.getData(index, addon.index).get(addon.index)

        val addonValue = (amount * (value / 100))
        e.amount += addonValue.toInt()
        if (addon.actionBar != null) {
            e.player.sendActionBar(toTip(addon.actionBar!!, amount, addonValue))
        }
        if (addon.message != null) {
            e.player.sendMessage(toTip(addon.message!!, amount, addonValue))
        }
    }

    fun toTip(string: String, amount: Int, addon: Double): String {
        return string.replace("{0}", amount.toString()).replace("{1}", addon.toString())
    }

    val addon = DefaultImpl()

    class DefaultImpl : Attribute.Entry() {

        val actionBar: String?
            get() = node.toRoot().getString("$name.action-bar")

        val message: String?
            get() = node.toRoot().getString("$name.message")

        override fun handler(memory: EventMemory, data: AttributeData.Data) {}
    }
}
