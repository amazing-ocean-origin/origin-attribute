package ac.github.oa.api.event.item

import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import taboolib.platform.type.BukkitProxyEvent

class ItemDecomposeEvent {

    class Pre(val player: Player, val itemStack: ItemStack) : BukkitProxyEvent()

    class Post(val player: Player, val itemStack: ItemStack, val products: List<ItemStack>) : BukkitProxyEvent()

}