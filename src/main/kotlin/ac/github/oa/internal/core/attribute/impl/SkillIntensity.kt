package ac.github.oa.internal.core.attribute.impl

import ac.github.oa.internal.base.event.EventMemory
import ac.github.oa.internal.core.attribute.AbstractAttribute
import ac.github.oa.internal.core.attribute.Attribute
import ac.github.oa.internal.core.attribute.AttributeData
import ac.github.oa.internal.core.attribute.AttributeType

object SkillIntensity : AbstractAttribute() {

    override val types: Array<AttributeType>
        get() = arrayOf(AttributeType.OTHER)

    val addon = object : Attribute.Entry() {

        override fun handler(memory: EventMemory, data: AttributeData.Data) {

        }

    }


}