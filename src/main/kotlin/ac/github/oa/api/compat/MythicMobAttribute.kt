package ac.github.oa.api.compat

import ac.github.oa.api.OriginAttributeAPI
import ac.github.oa.internal.core.attribute.AttributeData
import ac.github.oa.internal.core.attribute.TimeoutAttributeData
import io.lumine.xikage.mythicmobs.adapters.AbstractEntity
import io.lumine.xikage.mythicmobs.api.bukkit.events.MythicMechanicLoadEvent
import io.lumine.xikage.mythicmobs.io.MythicLineConfig
import io.lumine.xikage.mythicmobs.skills.ITargetedEntitySkill
import io.lumine.xikage.mythicmobs.skills.SkillMechanic
import io.lumine.xikage.mythicmobs.skills.SkillMetadata
import org.bukkit.entity.LivingEntity
import taboolib.common.platform.event.OptionalEvent
import taboolib.common.platform.event.SubscribeEvent
import taboolib.common.platform.function.info
import taboolib.common5.clong
import taboolib.common5.eqic
import java.util.UUID

class MythicMobAttribute(line: String, config: MythicLineConfig) : SkillMechanic(line, config), ITargetedEntitySkill {

    val id = config.getPlaceholderString("id", UUID.randomUUID().toString())
    val tick = config.getPlaceholderInteger(arrayOf("time", "t", "tick"), -1)
    val attributes = config.getPlaceholderString("s", "__EMPTY__")


    companion object {

        @SubscribeEvent(bind = "io.lumine.xikage.mythicmobs.api.bukkit.events.MythicMechanicLoadEvent")
        fun e(e: OptionalEvent) {
            val event = e.get<MythicMechanicLoadEvent>()
            if (event.mechanicName.eqic("oa-attr") || event.mechanicName.eqic("oa-attribute")) {
                event.register(MythicMobAttribute(event.container.configLine, event.config))
            } else if (event.mechanicName.eqic("oa-attack")) {
                event.register(MythicMobAttack(event.container.configLine, event.config))
            } else if (event.mechanicName.eqic("oa-dropitem")) {
                event.register(MythicMobDropOriginItem(event.container.configLine, event.config))
            }
        }

    }

    override fun castAtEntity(data: SkillMetadata, target: AbstractEntity): Boolean {
        if (!target.isDead || attributes.get() != "__EMPTY__") {
            val attributeData = TimeoutAttributeData(tick.get().clong)
            attributeData.merge(OriginAttributeAPI.loadList(attributes.get().split(",")))
            OriginAttributeAPI.setExtra(target.uniqueId, id.get(), attributeData)
            OriginAttributeAPI.callUpdate(target.bukkitEntity as LivingEntity)
            return true
        }
        return false
    }

}