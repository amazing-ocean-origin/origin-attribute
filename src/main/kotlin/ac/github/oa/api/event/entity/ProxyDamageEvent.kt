package ac.github.oa.api.event.entity

import ac.github.oa.internal.base.event.impl.DamageMemory
import ac.github.oa.internal.base.event.impl.customCause
import org.bukkit.entity.Arrow
import org.bukkit.entity.LivingEntity
import org.bukkit.entity.Projectile
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.entity.EntityDamageEvent
import taboolib.platform.type.BukkitProxyEvent
import taboolib.platform.util.attacker
import taboolib.type.BukkitEquipment
import java.util.UUID

class ProxyDamageEvent(val origin: EntityDamageByEntityEvent) : BukkitProxyEvent() {

    var id = UUID.randomUUID()

    var damager = origin.damager

    var entity = origin.entity as LivingEntity

    var damage = origin.damage

    var attacker = origin.attacker ?: damager as LivingEntity


    val isProjectile: Boolean
        get() = damager is Projectile

    val projectile: Projectile? = damager as? Projectile

    val isArrow = projectile is Arrow

    val bukkitCause = origin.cause

    val data = mutableMapOf<String, Any>()

    // data > bukkitCause > hand > default
    var customCause = if (data.containsKey("cause")) {
        data["cause"]!!.toString()
    } else if (origin.cause == EntityDamageEvent.DamageCause.MAGIC) {
        "magic"
    }
    else {
        BukkitEquipment.HAND.getItem(attacker)?.customCause ?: "physics"
    }

    fun createDamageContext(): DamageMemory {
        return DamageMemory(this)
    }

}