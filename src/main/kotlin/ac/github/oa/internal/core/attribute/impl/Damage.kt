package ac.github.oa.internal.core.attribute.impl

import ac.github.oa.internal.base.event.EventMemory
import ac.github.oa.internal.base.event.impl.DamageMemory
import ac.github.oa.internal.base.event.impl.isMagic
import ac.github.oa.internal.base.event.impl.isPhysics
import ac.github.oa.internal.base.event.impl.isProjectile
import ac.github.oa.internal.core.attribute.AbstractAttribute
import ac.github.oa.internal.core.attribute.Attribute
import ac.github.oa.internal.core.attribute.AttributeData
import ac.github.oa.internal.core.attribute.AttributeType
import org.bukkit.entity.Mob
import org.bukkit.entity.Player

object Damage : AbstractAttribute() {

    override val types: Array<AttributeType>
        get() = arrayOf(AttributeType.ATTACK)

    val physical = object : Attribute.Entry() {

        override val type: Attribute.Type
            get() = Attribute.Type.RANGE

        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            memory as DamageMemory
            if (memory.isPhysics && !memory.hasDamage(this)) {
                memory.addDamage(this,data.get(this))
            }
        }

    }

    val magic = object : Attribute.Entry() {

        override val type: Attribute.Type
            get() = Attribute.Type.RANGE

        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            memory as DamageMemory
            if (memory.isMagic && !memory.hasDamage(this)) {
                memory.addDamage(this,data.get(this))
            }
        }

    }

    val addon = DefaultAddonImpl()

    val pvpDamage = object : Attribute.Entry() {
        override val type: Attribute.Type
            get() = Attribute.Type.RANGE

        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            memory as DamageMemory
            if (memory.injured is Player) {
                memory.addDamage(this, data.get(0))
            }
        }
    }

    val pveDamage = object : Attribute.Entry() {
        override val type: Attribute.Type
            get() = Attribute.Type.RANGE

        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            memory as DamageMemory
            if (memory.injured !is Player) {
                memory.addDamage(this, data.get(0))
            }
        }
    }

    class DefaultAddonImpl : Attribute.Entry() {

        override val type: Attribute.Type
            get() = Attribute.Type.SINGLE


        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            memory as DamageMemory
            memory.dynamicDamageSource {
                if (it.value > 0) {
                    it.value += (it.value * (data.get(0) / 100))
                }
            }
        }

    }
}
