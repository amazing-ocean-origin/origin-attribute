package ac.github.oa.api.event.item

import ac.github.oa.internal.core.item.Item
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import taboolib.platform.type.BukkitProxyEvent

class ItemTakenEvent(val player: Player, val item: Item, val items: List<ItemStack>) : BukkitProxyEvent() {
}