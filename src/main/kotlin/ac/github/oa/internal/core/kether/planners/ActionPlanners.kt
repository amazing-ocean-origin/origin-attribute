package ac.github.oa.internal.core.kether.planners

import ac.github.oa.util.NAMESPACE
import ac.github.oa.util.player
import com.bh.planners.api.PlannersAPI
import taboolib.module.kether.KetherParser
import taboolib.module.kether.actionNow
import taboolib.module.kether.script
import taboolib.module.kether.scriptParser

object ActionPlanners {

    /**
     * planners cast skill level
     */
    @KetherParser(["planners"], namespace = NAMESPACE, shared = true)
    fun parser() = scriptParser {
        actionNow {
            when(it.nextToken()) {
                "cast" -> {
                    val skill = it.nextToken()
                    actionNow {
                        val player = this.script().player()
                        PlannersAPI.directCast(player, skill, it.nextInt())
                    }
                }
                else -> {}
            }
        }
    }


}