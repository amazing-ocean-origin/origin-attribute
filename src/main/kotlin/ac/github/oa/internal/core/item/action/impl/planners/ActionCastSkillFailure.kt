package ac.github.oa.internal.core.item.action.impl.planners

import ac.github.oa.internal.core.item.action.IActionEvent
import ac.github.oa.internal.core.item.action.Source
import com.bh.planners.api.event.PlayerCastSkillEvents
import org.bukkit.entity.Player
import taboolib.module.kether.ScriptContext

@Source("Planners")
object ActionCastSkillFailure : IActionEvent<PlayerCastSkillEvents.Failure>() {
    override val namespace: String
        get() = "on cast skill failure"
    override val event: Class<PlayerCastSkillEvents.Failure>
        get() = PlayerCastSkillEvents.Failure::class.java

    override fun test(e: PlayerCastSkillEvents.Failure): Player {
        return e.player
    }

    override fun inject(context: ScriptContext, e: PlayerCastSkillEvents.Failure) {
        context.rootFrame().variables()["skill"] = e.skill.key
    }

}