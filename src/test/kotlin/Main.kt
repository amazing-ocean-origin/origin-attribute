import ac.github.oa.api.common.Matcher
import ac.github.oa.api.common.script.StringScript

class Main {

    companion object {

        val parse = "攻击力 10"

        @JvmStatic
        fun main(args: Array<String>) {
            val matcher = Matcher.parse(parse) as Matcher.Single
            val millis = System.currentTimeMillis()
            for (i in 1..10000) {
                matcher.initial()
            }
            println("Time1 ${System.currentTimeMillis() - millis}/ms")

            val loreMap = LoreMap<TestData>(true, true, true);
            loreMap.put("攻击力",TestAData())
            val bMillis = System.currentTimeMillis()
            for (i in 1..10000) {
                loreMap.getMatchResult(parse)
            }
            println("Time2 ${System.currentTimeMillis() - bMillis}/ms")



        }

    }

    interface TestData {

    }

    class TestAData : TestData


}
