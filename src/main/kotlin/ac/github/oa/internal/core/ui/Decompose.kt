package ac.github.oa.internal.core.ui

import ac.github.oa.api.event.plugin.OriginPluginReloadEvent
import org.bukkit.inventory.ItemStack
import taboolib.common.platform.event.SubscribeEvent
import taboolib.library.configuration.ConfigurationSection
import taboolib.library.xseries.getItemStack
import taboolib.module.configuration.Config
import taboolib.module.configuration.Configuration

object Decompose {

    @Config("decompose.yml", migrate = true)
    lateinit var config: Configuration

    @SubscribeEvent
    fun e(e: OriginPluginReloadEvent) {
        config.reload()
    }

    val root: ConfigurationSection
        get() = config.getConfigurationSection("decompose")!!

    val title: String
        get() = root.getString("title")!!

    val rows: Int
        get() = root.getInt("rows", 3)

    //slots
    val confirm: Int
        get() = root.getInt("confirm.slot")

    val show: List<Int>
        get() = root.getIntegerList("show.slots")

    val slot: Int
        get() = root.getInt("slot")

    //icon
    val confirmIcon: ItemStack
        get() = root.getItemStack("confirm")!!

    val getIcon: ItemStack
        get() = root.getItemStack("get")!!

    //默认材料
    val default: String?
        get() = root.getString("default")

}