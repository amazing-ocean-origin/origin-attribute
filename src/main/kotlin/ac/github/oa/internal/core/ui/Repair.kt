package ac.github.oa.internal.core.ui

import ac.github.oa.api.event.plugin.OriginPluginReloadEvent
import ac.github.oa.internal.core.ui.Repair.allInfo
import org.bukkit.entity.Player
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.inventory.ItemStack
import taboolib.common.platform.event.SubscribeEvent
import taboolib.common5.Baffle
import taboolib.library.configuration.ConfigurationSection
import taboolib.library.xseries.getItemStack
import taboolib.module.configuration.Config
import taboolib.module.configuration.Configuration
import taboolib.platform.compat.PlaceholderExpansion
import java.util.concurrent.TimeUnit

object Repair {


    @Config("repair.yml", migrate = true)
    lateinit var config: Configuration

    @SubscribeEvent
    fun e(e: OriginPluginReloadEvent) {
        config.reload()
    }

    val baffle = Baffle.of(200, TimeUnit.MILLISECONDS)
    @SubscribeEvent
    fun onQuit(e: PlayerQuitEvent) {
        baffle.reset(e.player.name)
    }

    val allInfo = mutableMapOf<Player, RepairInfo>()

    val root: ConfigurationSection
        get() = config.getConfigurationSection("repair")!!

    val title: String
        get() = root.getString("title")!!

    val rows: Int
        get() = root.getInt("rows", 3)

    //slots
    val confirm: Int
        get() = root.getInt("confirm.slot")

    val protect: List<Int>
        get() = root.getIntegerList("protect.slots")

    val repair: Int
        get() = root.getInt("repair.slot")

    val save: List<Int>
        get() = root.getIntegerList("save.slots")

    //icon
    val confirmIcon: ItemStack
        get() = root.getItemStack("confirm")!!


}

object OriginPlaceholder : PlaceholderExpansion {

    override val identifier: String
        get() = "repair"

    override fun onPlaceholderRequest(player: Player?, args: String): String {
        if (player != null) {
            when (args) {
                "money" -> return (allInfo[player]?.money?:"0").toString()
                "points" -> return (allInfo[player]?.points?:"0").toString()
                "save-percent" -> return (allInfo[player]?.save?:"1").toString()
                "protect-percent" -> return (allInfo[player]?.protect?:"1").toString()
                "new-durability" -> return (allInfo[player]?.newDurability?:"0").toString()
                "all-money" -> return (allInfo[player]?.allMoney?:"0").toString()
                "all-points" -> return (allInfo[player]?.allPoints?:"0").toString()
                "durability" -> return (allInfo[player]?.durability?:"0").toString()
            }
        }
        return "N/O"
    }

}
