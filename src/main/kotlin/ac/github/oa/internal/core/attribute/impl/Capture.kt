package ac.github.oa.internal.core.attribute.impl

import ac.github.oa.api.event.entity.EntityDamageEvent
import ac.github.oa.internal.base.enums.PriorityEnum
import ac.github.oa.internal.base.event.EventMemory
import ac.github.oa.internal.base.event.impl.DamageMemory
import ac.github.oa.internal.core.attribute.*
import taboolib.common.platform.event.SubscribeEvent
import taboolib.common.util.random
import kotlin.math.min

object Capture : AbstractAttribute() {


    @SubscribeEvent(ignoreCancelled = true)
    fun onCapture(e: EntityDamageEvent) {
        if (e.priorityEnum == PriorityEnum.POST) {
            val damageMemory = e.damageMemory
            if (damageMemory.labels["@Capture"] == true) {

                val attacker = damageMemory.attacker
                val injured = damageMemory.injured
                // 目标b偏移向目标a
                val locationA = injured.location.clone()
                val locationB = attacker.location.clone()
                locationA.pitch = 0f
                locationB.pitch = 0f
                val distance = locationA.distance(locationB)
                val vectorAB = locationB.clone().subtract(locationA).toVector()
                vectorAB.length()
                val step = min(capture.step, distance)
                injured.velocity = vectorAB.multiply(step)
            }
        }
    }

    override val types: Array<AttributeType>
        get() = arrayOf(AttributeType.ATTACK)

    val capture = DefaultImpl()

    class DefaultImpl : Attribute.Entry() {

        override val type: Attribute.Type
            get() = Attribute.Type.SINGLE

        val step: Double
            get() = node.toRoot().getDouble("${name}.step")


        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            memory as DamageMemory
            memory.setLabel("@Capture", random(data.get(this) / 100))
        }

    }

}
