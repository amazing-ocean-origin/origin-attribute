package ac.github.oa.api.compat

import com.bh.planners.api.PlannersAPI.plannersProfile
import com.bh.planners.core.module.mana.ManaManager
import org.bukkit.Bukkit
import org.bukkit.entity.Player

object PlannersHook {

    val enable = Bukkit.getPluginManager().isPluginEnabled("Planners")

    fun manaHeal(player: Player) {
        if (enable) {
            val profile = player.plannersProfile
            val manaManager = ManaManager.INSTANCE
            manaManager.setMana(profile, manaManager.getMaxMana(profile))
        }
    }

}