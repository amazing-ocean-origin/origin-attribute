package ac.github.oa.api.common.script.action

import ac.github.oa.api.common.script.InlineScript
import ac.github.oa.api.common.script.InlineScriptContext

object ScriptMap : InlineScript {
    override val id: String
        get() = "map"

    override fun handleScript(argument: String, context: InlineScriptContext): String? {
        return context.data[argument]?.toString() ?: "$argument null"
    }
}