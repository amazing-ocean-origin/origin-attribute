package ac.github.oa.internal.core.item.generator

import ac.github.oa.api.common.script.InlineScriptContext
import ac.github.oa.internal.core.item.Item
import ac.github.oa.internal.core.item.ItemPlant
import ac.github.oa.util.random
import org.bukkit.entity.LivingEntity
import org.bukkit.inventory.ItemStack
import taboolib.common.LifeCycle
import taboolib.common.platform.Awake
import taboolib.module.configuration.util.getMap

@Awake(LifeCycle.LOAD)
class ProxyGenerator : ItemGenerator {

    override val name: String
        get() = "proxy"

    private fun Item.vars(): Map<String, Any> {
        return config.getMap("vars")
    }

    private fun Item.id(): String {
        return config.getString("id")!!
    }

    override fun build(entity: LivingEntity?, item: Item, map: MutableMap<String, String>): ItemStack {
        val context = InlineScriptContext(entity)
        map.putAll(item.vars().map { it.key to it.value.random(context) })
        val id = item.id()
        return ItemPlant.build(entity, id, map) ?: error("Item $id not found.")
    }
}