package ac.github.oa.internal.core.attribute

import ac.github.oa.api.compat.OriginPlaceholder
import ac.github.oa.internal.base.event.EventMemory
import org.bukkit.entity.LivingEntity
import org.bukkit.entity.Player
import taboolib.library.configuration.ConfigurationSection

interface Attribute {

    fun onLoad()

    fun onReload()

    fun onDisable()

    fun toName(): String

    fun toLocalName(): String

    fun getEntry(index: Int): Entry

//    fun handleParse(string: String)

    fun toValue(player: Player,attributeData: AttributeData,args: Array<String>) : Any?

    fun getEntry(name: String): Entry

    fun toEntrySize(): Int

    fun getPriority(): Int

    fun setPriority(index: Int)

    fun toRoot(): ConfigurationSection


    abstract class Entry {

        var index: Int = -1

        open val type = Type.SINGLE

        open lateinit var name: String

        open lateinit var node: Attribute

        val option : ConfigurationSection?
            get() = node.toRoot().getConfigurationSection(name)

        open var combatPower: Double = 0.0

        var ignores : List<String> = emptyList()

        var keywords : List<String> = emptyList()

        var corrects : List<List<Double>> = emptyList()

        @Suppress("UNCHECKED_CAST")
        open fun onEnable() {
            combatPower = option?.getDouble("combat-power") ?: 0.0
            ignores = option?.getStringList("ignores") ?: emptyList()
            keywords = option?.getStringList("keywords") ?: emptyList()
            if (option?.contains("correct") == true) {
                corrects = option?.getMapList("correct") as List<List<Double>>
            }

        }

        abstract fun handler(memory: EventMemory, data: AttributeData.Data)

        open fun toValue(entity: LivingEntity, args: String, data: AttributeData.Data): Any? {
            if (type == Type.SINGLE || type == Type.PERCENT) {
                return data.get(0)
            }
            if (type == Type.RANGE) {
                return when (args) {
                    "max" -> data.get(1)
                    "min" -> data.get(0)
                    "random" -> data.random()
                    else -> "${OriginPlaceholder.df2.format(data.get(0))} - ${OriginPlaceholder.df2.format(data.get(1))}"
                }
            }
            return "N/O"
        }

        override fun toString(): String {
            return "Entry(index=$index, type=$type, name='$name')"
        }


    }



    enum class Type(val size: Int) {
        SINGLE(1), RANGE(2),PERCENT(3)
    }

}
