package ac.github.oa.internal.core.item.action.impl.planners

import ac.github.oa.internal.core.item.action.IActionEvent
import ac.github.oa.internal.core.item.action.Source
import com.bh.planners.api.event.PlayerCastSkillEvents
import org.bukkit.entity.Player
import taboolib.module.kether.ScriptContext

@Source("Planners")
object ActionCastSkillRecord : IActionEvent<PlayerCastSkillEvents.Record>() {

    override val namespace: String
        get() = "on cast skill record"
    override val event: Class<PlayerCastSkillEvents.Record>
        get() = PlayerCastSkillEvents.Record::class.java

    override fun test(e: PlayerCastSkillEvents.Record): Player {
        return e.player
    }

    override fun inject(context: ScriptContext, e: PlayerCastSkillEvents.Record) {
        context.rootFrame().variables()["skill"] = e.skill.key
    }

}