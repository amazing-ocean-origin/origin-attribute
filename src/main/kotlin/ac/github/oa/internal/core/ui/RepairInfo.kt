package ac.github.oa.internal.core.ui

class RepairInfo {

    var money: Double = 0.0
    var points: Int = 0

    var allMoney: Double = 0.0
    var allPoints: Int = 0

    var save: Double = 0.0
    var protect: Double = 0.0

    var newDurability: Int = 0
    var durability: Int = 0

}