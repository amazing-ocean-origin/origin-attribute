package ac.github.oa.api.event.item

import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import taboolib.platform.type.BukkitProxyEvent

class ItemDurabilityDamageEvent {

    class Pre(val player: Player?, val itemStack: ItemStack, val max: Int, val old: Int, var value: Int, val source: Any? = null) : BukkitProxyEvent()

    class Post(val player: Player?, val itemStack: ItemStack,val max: Int, val old: Int,var value: Int, val source: Any? = null) : BukkitProxyEvent()


}