package ac.github.oa.api.compat

import org.black_ixx.playerpoints.PlayerPoints
import org.black_ixx.playerpoints.PlayerPointsAPI
import org.bukkit.Bukkit
import taboolib.common.LifeCycle
import taboolib.common.platform.Awake

object PlayerPointsHook {

    var PlayerPointsAPI: PlayerPointsAPI? = null

    @Awake(LifeCycle.ENABLE)
    fun hook() {
        if (Bukkit.getPluginManager().isPluginEnabled("PlayerPoints")) {
            PlayerPointsAPI = PlayerPoints.getInstance().api;
        }
    }

}