package ac.github.oa.api.common.script.action

import ac.github.oa.api.common.script.InlineScript
import ac.github.oa.api.common.script.InlineScriptContext
import org.bukkit.entity.Player
import taboolib.common.platform.function.warning
import taboolib.platform.compat.replacePlaceholder

object ScriptPlaceholder : InlineScript {
    override val id: String
        get() = "papi"

    override fun handleScript(argument: String, context: InlineScriptContext): String? {
        val split = argument.split("def:")
        val def = if (split.size == 2) split[1] else null
        return if (context.sender is Player) {
            split[0].replacePlaceholder(context.sender)
        } else if (def != null) {
            def
        } else {
            warning("{papi:<expression>def:<default>} not found for default the '$argument'")
            "null"
        }
    }


}