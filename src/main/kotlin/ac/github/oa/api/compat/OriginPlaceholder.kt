package ac.github.oa.api.compat

import ac.github.oa.OriginAttribute
import ac.github.oa.api.OriginAttributeAPI
import ac.github.oa.internal.core.attribute.AttributeManager
import org.bukkit.entity.Player
import taboolib.common5.cint
import taboolib.common5.format
import taboolib.platform.BukkitPlugin
import taboolib.platform.compat.PlaceholderExpansion
import java.text.DecimalFormat

object OriginPlaceholder : PlaceholderExpansion {

    init {
        BukkitPlugin.getInstance().logger.info("|- PlaceholderAPI plugin hooked.")
    }

    val df2 = DecimalFormat("0.00");

    override val identifier: String
        get() = "rpg"


    fun toLocal(value: Double): String {
        return if (value > 9999) {
            "${(value / 10000).format()}w"
        } else value.format().toString()
    }

    override fun onPlaceholderRequest(player: Player?, args: String): String {
        val split = args.split(":").toTypedArray()
        val key = split[0]
        val attributeData = OriginAttributeAPI.getAttributeData(player!!)

        if (split[0].startsWith("combat-power")) {
            return when (split[0]) {
                "combat-power-i" -> return attributeData.combatPower.cint.toString()
                "combat-power-local" -> return toLocal(attributeData.combatPower)
                "combat-power-scale2" -> return attributeData.combatPower.format(2).toString()
                else -> attributeData.combatPower.toString()
            }
        } else if (args == "health") {
            return df2.format(player.health)
        } else if (args == "max-health") {
            return df2.format(player.maxHealth)
        }

        val attribute = AttributeManager.usableAttributes.values.firstOrNull { it.toName() == key }
        if (attribute != null) {

            val value = attribute.toValue(player, attributeData, split)
            return if (value is Double) {
                OriginAttribute.decimalFormat.format(value)
            } else {
                value.toString()
            }
        }
        return "N/O"
    }
}
