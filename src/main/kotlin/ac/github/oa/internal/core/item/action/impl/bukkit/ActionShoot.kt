package ac.github.oa.internal.core.item.action.impl.bukkit

import ac.github.oa.internal.core.item.action.IActionEvent
import org.bukkit.entity.Player
import org.bukkit.event.entity.EntityShootBowEvent
import taboolib.module.kether.ScriptContext

object ActionShoot : IActionEvent<EntityShootBowEvent>() {

    override val namespace: String
        get() = "on shoot"
    override val event: Class<EntityShootBowEvent>
        get() = EntityShootBowEvent::class.java

    override fun test(e: EntityShootBowEvent): Player? {
        return e.entity as? Player ?: return null
    }

    override fun inject(context: ScriptContext, e: EntityShootBowEvent) {
        context.rootFrame().variables()["bow"] = e.bow
        context.rootFrame().variables()["consumable"] = e.consumable
        context.rootFrame().variables()["projectile"] = e.projectile
    }

}