package ac.github.oa.internal.core.item

import ac.github.oa.internal.core.item.action.ItemAction
import org.bukkit.entity.LivingEntity
import org.bukkit.inventory.ItemStack
import taboolib.library.configuration.ConfigurationSection

class Item(val config: ConfigurationSection) {

    // id
    val key = config.name

    // 是否开启动态更新
    val isUpdate = config.getBoolean("update", false)

    // 标注版本
    val hasCode = config.getValues(false).hashCode()

    // 生成器
    val generator = config.getString("g", "")!!

    val isClearDefault = config.getBoolean("clear-default")

    val id = config.getString("id", "stone")!!
    val name = config.getString("name", "__null__")!!
    val lore = config.getStringList("lore")
    val isUnBreakable = config.getBoolean("unbreakable")
    val enchantments = config.getStringList("enchantments")
    val itemFlags = config.getStringList("item-flags")
    val damage = config.getString("data", "0")!!
    val skillOwner = config.getString("skull-owner")
    val skillTexture = config.getString("skull-texture")
    val color = config.getString("color")
    val attackSpeed = config.getString("attack-speed")

    val decompose = config.getStringList("decompose")

    val repair = Repair(config.getConfigurationSection("repair"))

    class Repair(val root: ConfigurationSection?) {
        val type = root?.getString("type", "unbreak") ?: "unbreak"
        val money = root?.getDouble("money", 1.0) ?: 1.0
        val points = root?.getInt("points", 0) ?: 0
        val protect = root?.getDouble("protect", 0.05) ?: 0.05
        val save = root?.getDouble("save", 1.0) ?: 1.0
    }

    val actions = config.getMapList("actions").map {
        val list = when (val any = it["action"] ?: listOf<String>()) {
            is String -> listOf(any.toString())
            is List<*> -> any.map { it.toString() }
            else -> listOf()
        }
        ItemAction(it["\$e"].toString(), list)
    }

    // 创建
    fun create(entity: LivingEntity?, options: MutableMap<String, String> = mutableMapOf()): ItemStack? {
        return ItemPlant.build(entity, key, options)
    }


    fun builder() = ItemBuilder(this)

}