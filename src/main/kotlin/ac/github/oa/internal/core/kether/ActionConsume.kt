package ac.github.oa.internal.core.kether

import ac.github.oa.listener.OnListener
import ac.github.oa.util.NAMESPACE
import ac.github.oa.util.itemInstance
import org.bukkit.Bukkit
import taboolib.module.kether.*

object ActionConsume {

    /**
     * consume 1
     */
    @KetherParser(["consume"], namespace = NAMESPACE, shared = true)
    fun parser() = scriptParser {

        val number = it.nextInt()

        actionFuture {
            val item = script().itemInstance().itemStack
            item.amount = if (item.amount > number) (item.amount - number) else 0
            OnListener.asyncUpdate(Bukkit.getPlayer(player().uniqueId)!!)
            it.complete(item.amount)
        }

    }

}