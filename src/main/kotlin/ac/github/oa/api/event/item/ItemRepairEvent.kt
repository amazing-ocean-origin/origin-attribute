package ac.github.oa.api.event.item

import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import taboolib.platform.type.BukkitProxyEvent

class ItemRepairEvent {

    class Pre(val player: Player?, val itemStack: ItemStack, val money: Double, val points: Int, val new: Int, val old: Int, var oldmax: Int) : BukkitProxyEvent()

    class Post(val player: Player?, val itemStack: ItemStack, val money: Double, val points: Int, val new: Int, val old: Int, var oldmax: Int) : BukkitProxyEvent()

    class Failure(val player: Player?, val itemStack: ItemStack, val money: Double, val points: Int) : BukkitProxyEvent()

    class Lowest(val player: Player?, val itemStack: ItemStack) : BukkitProxyEvent()

}