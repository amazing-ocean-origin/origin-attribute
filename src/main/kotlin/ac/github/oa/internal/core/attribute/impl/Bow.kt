package ac.github.oa.internal.core.attribute.impl

import ac.github.oa.internal.base.event.EventMemory
import ac.github.oa.internal.base.event.impl.DamageMemory
import ac.github.oa.internal.base.event.impl.isArrow
import ac.github.oa.internal.base.event.impl.isMagic
import ac.github.oa.internal.base.event.impl.isPhysics
import ac.github.oa.internal.core.attribute.AbstractAttribute
import ac.github.oa.internal.core.attribute.Attribute
import ac.github.oa.internal.core.attribute.AttributeData
import ac.github.oa.internal.core.attribute.AttributeType
import taboolib.common5.Coerce

object Bow : AbstractAttribute() {

    override val types: Array<AttributeType>
        get() = arrayOf(AttributeType.ATTACK, AttributeType.DEFENSE)

    val attack = object : Attribute.Entry() {

        override val type: Attribute.Type
            get() = Attribute.Type.RANGE

        // 继承物理攻击
        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            memory as DamageMemory
            if (memory.isPointerAttack() && memory.isArrow) {
                val get = data.get(this)
                memory[this] = get
                memory.damageSources<Any>().forEach {
                    if (memory.isPhysics && it.any == Damage.physical) {
                        it.value += get
                    }
                    if (memory.isMagic && it.any == Damage.magic) {
                        it.value += get
                    }
                }
            }

        }

    }

    val defence = object : Attribute.Entry() {

        override val type: Attribute.Type
            get() = Attribute.Type.RANGE

        override fun handler(memory: EventMemory, data: AttributeData.Data) {
            memory as DamageMemory
            if (memory.isPointerInjured() && memory.isArrow) {
                val value = Coerce.toDouble(memory.labels[attack])
                if (value > 0) {
                    memory.addDamage(this, -data.get(this))
                }
            }
        }

    }

}