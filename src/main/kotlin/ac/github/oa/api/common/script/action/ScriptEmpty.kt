package ac.github.oa.api.common.script.action

import ac.github.oa.api.common.script.InlineScript
import ac.github.oa.api.common.script.InlineScriptContext

object ScriptEmpty : InlineScript {
    override val id: String
        get() = "empty"

    override fun handleScript(argument: String, context: InlineScriptContext): String? {
        return "%del%"
    }
}