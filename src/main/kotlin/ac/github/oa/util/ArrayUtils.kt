package ac.github.oa.util

import ac.github.oa.api.common.script.InlineScriptContext
import ac.github.oa.internal.core.attribute.AttributeData
import org.bukkit.entity.LivingEntity

object ArrayUtils {

    /**
     * damage +{eval:{map:value}*10+2.0}
     */
    fun read(entity: LivingEntity, data: AttributeData, strings: List<String>, amount: Double): List<String> {
        val context = InlineScriptContext(entity)
        context.data["data"] = data
        return read(strings, amount, context)
    }

    fun read(entity: LivingEntity, strings: List<String>, amount: Double): List<String> {
        return read(strings, amount, InlineScriptContext(entity))
    }

    fun read(strings: List<String>, amount: Double, context: InlineScriptContext): List<String> {
        context.data["value"] = amount
        return strings.random(context)
    }
}
